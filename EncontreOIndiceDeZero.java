
public class EncontreOIndiceDeZero {
	public static void main(String[] args) {
		int[] seq = { 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1}; //insira a entrada aqui
		int cont = 0;
		int contagemTemp = 0;
		int contagem = 0;
		int indexZero=-1;
		
		for(int i=0; i<seq.length; i++)
		{
			if(seq[i] == 0)
			{
				seq[i] = 1;
				for(int j=0; j<seq.length; j++)
				{
					if(seq[j] == 1)
					{
						cont++;
						if(cont>contagemTemp)
						{
							contagemTemp = cont;
						}
						
					}else
					{
						cont=0;
					}
				}
				seq[i] = 0;
				if(contagemTemp > contagem)
				{
					contagem = contagemTemp;
					indexZero = i;
				}
				
			}
		}
		
		//considerando que o indice comeca em 1
		System.out.println(indexZero+1);
	}
}
